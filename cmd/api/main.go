package main

import (
	_ "avito_assignment/docs"
	"avito_assignment/internal/config"
	"avito_assignment/internal/infrastructure"
	"avito_assignment/internal/repository"
	"avito_assignment/internal/repository/postgres"
	"avito_assignment/internal/service"
	"avito_assignment/internal/transport/rest/handler"
	"context"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/swagger"
	"github.com/robfig/cron"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"time"
)

// @title User segment service description
// @version 2.0
// @description http service

// @host localhost:3000
// @BasePath /
// @schemes http

func main() {

	if err := SetupViper(); err != nil {
		log.Fatal(err.Error())
	}

	log.Info("viper OK")

	app := fiber.New()

	app.Get("/swagger/*", swagger.HandlerDefault)
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)

	pgConfig, err := config.GetDBConfig()

	if err != nil {
		log.Fatal(err.Error())
	}

	log.Info("got bd config")

	db, err := infrastructure.SetUpPostgresDatabase(ctx, pgConfig)

	if err != nil {
		log.Fatal(err.Error())
	}

	log.Info("connected to db")

	minioConfig, err := config.GetMinioConfig()

	if err != nil {
		log.Fatal(err.Error())
	}

	log.WithFields(log.Fields{}).Info("s3 config")

	s3, err := infrastructure.SetUpS3Storage(minioConfig)

	if err != nil {
		log.Fatal(err.Error())
	}

	log.Info("connected to s3")

	path, err := config.GetLocalConfig()
	if err != nil {
		log.Fatal(err.Error())
	}

	segmentRepository := postgres.NewSegmentRepository(db)
	localRepository := repository.NewLocalRepository(path.Path)
	minioRepository := repository.NewMinioRepository(s3, minioConfig.S3_BUCKET)

	segmentService := service.NewSegmentService(segmentRepository, localRepository, minioRepository)
	segmentHandler := handler.NewSegmentHandler(segmentService)

	segmentHandler.InitRoutes(app)

	port := viper.GetString("http.port")
	if err = app.Listen(":" + port); err != nil {
		log.Fatal(err)
	}

	log.Info("server is running")

	c := cron.New()
	err = c.AddFunc(viper.Get("expiredData.period").(string), segmentService.ControlExpiredUsers)
	if err != nil {
		log.Info(err.Error())
	}
}

func SetupViper() error {

	viper.SetConfigType("yaml")
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")

	if err := viper.ReadInConfig(); err != nil {
		return err
	}

	return nil
}
