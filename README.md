# Сервис динамического сегментирования пользователей


## Инструкция по запуску
_Запуск состоит из нескольких шагов, т.к. сервис зависит от PostgreSQL и minio_
```
version: '3.8'

services:
  avito_assignment:
    build:
      context: .
      dockerfile: Dockerfile
    ports:
    - "3000:3000"
    environment:
      DB_USERNAME: "postgres"
      DB_PASSWORD: "postgres"
      DB_DB: "avito"
      DB_HOST: "db"
      DB_PORT: "5432"
    depends_on:
      - db
      - s3
    networks:
      - fullstack
  db:
    image: "postgres:alpine"
    container_name: "db"
    volumes:
      - "database_postgres:/var/lib/postgresql/data"
    ports:
      - "5432:5432"
    environment:
      POSTGRES_DB: "avito"
      POSTGRES_USER: "postgres"
      POSTGRES_PASSWORD: "postgres"
      POSTGRES_PORT: "5432"
    networks:
      - fullstack
  s3:
    image: "minio/minio:latest"
    command: server --console-address ":9001" /data/
    ports:
      - "9000:9000"
      - "9001:9001"
    volumes:
      - "storage_minio:/var/lib/minio/data"
    container_name: "minio"
    networks:
      - fullstack
    environment:
      MINIO_ROOT_USER: avitotech
      MINIO_ROOT_PASSWORD: minio123
    healthcheck:
      disable: true

volumes:
  database_postgres:
  storage_minio:

networks:
  fullstack:
    driver: "bridge"
```
1. Запуск всех сервисов
```
docker compose up --build avito_assignment
```
2. Утилита объектного minio (для отчетов)
```
go get github.com/minio/mc
```
Логин
```
mc alias set minio http://localhost:9000 avitotech minio123
```
Данные из docker-compose.yml:
```
      MINIO_ROOT_USER: avitotech
      MINIO_ROOT_PASSWORD: minio123
```
Создание bucket'а для отчетов:
```
mc mb minio/reports
```
3. Восстановление базы данных PostgreSQL:
```
cat avito_nodata.sql | docker exec -i db psql -U postgres
```


## Описание методов

### Создать сегмент


```
POST localhost:3000/segments
Content-Type: application/json

{
  "name": "toys",
  "percent": 70
}
```

Ответом может быть

```
{
  "createdSegment": "toys"
}
```



### Удалить сегмент

```
DELETE localhost:3000/segments/kittens
```

Ответом может быть

```
{
  "DeletedSegment": "kittens"
}
```

### Добавить пользователя в сегмент

```
POST localhost:3000/segment/addUser
Content-Type: application/json

{
  "add": ["cars"],
  "delete": ["computers"],
  "userId": 1
}
```

_И "add", и "delete" - обязательные поля. Можно оставить один из массивов пустым._ 

**_Пример ответа:_**

```
{
  "added": [
    "cars"
  ],
  "deleted": [
    "computers"
  ],
  "id": 1
}
```
_Если нужно добавить пользователя в сегмент на определенное время:_
```
POST localhost:3000/segment/addUser
Content-Type: application/json

{
  "add": ["cars"],
  "delete": [],
  "userId": 1,
  "time": "2023-09-11T00:00:00.000-00:00",
}
```



### Получить все активные сегменты пользователя

```
GET localhost:3000/segments/1
```

**_Пример ответа:_**

```
{
  "active segments": [
    {
      "name": "cars",
      "percent": 60
    },
    {
      "name": "computers",
      "percent": 0
    }
  ]
}
```

### Создать отчет по пользователю за определенный период:
```
GET localhost:3000/segments/report/4/2023-09
```
_**Пример ответа**_
```
"link": ""http://localhost:9000/reports/user4_report_2023-09.csv?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=avitotech%2F20230906%2Fus-east-1%2Flocalhost%2Faws4_request&X-Amz-Date=20230906T220144Z&X-Amz-Expires=7200&X-Amz-SignedHeaders=host&response-content-disposition=attachment%3B%20filename%3D%22user4_report_2023-09.csv%22&X-Amz-Signature=07fa92b18214a5a4d1e50643dfac8c809072c33af6c641aeff59063a67f0134d""
```

## Отчет об ошибках

### Описание
Метод создания отчета по пользователю за определнный период возвращает presigned URL на скачивание файла из minio, но вместо .csv отчета скачивается html-страница, которая не содержит отчет.
### Шаги 
1. Получение presogned link на нужный файл в minio:
```
	link, err := repository.S3.PresignedGetObject(ctx, repository.S3_bucket, newName, time.Duration(2)*time.Hour, reqParams)
```
2. Получение URL
```
link.String()
```
3. Замена алиасов на настоящий адрес
```
   link = strings.Replace(link, "s3", "localhost", -1)
   link = strings.Replace(link, "9000", "9001", -1)
```
### Ожидаемый результат
При переходе по ссылке скачивается csv файл отчета
### Настоящий результат
Скачивается html-страница, не содержащая отчет
### Возможная причина ошибки
Некорректная замена алиасов сервисов
### Возможные альтернативные решения
1. При отправке запроса на отчет вместо получения ссылки на его скачивание получать json с результатом и скачать файл в локальное устройство польозвателя.
Путь для скачивания задать в config.yml
2. Пользоваться другим объектным хранилищем с открытым API и получать глобальную ссылку на файл
3. Возвращать отчет в json (◐ω◑ )

