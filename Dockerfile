FROM golang
USER root
SHELL ["/bin/bash", "-c"]

RUN go version
RUN mkdir -p /usr/applications/avito_assignment
WORKDIR /usr/applications/avito_assignment

COPY . /usr/applications/avito_assignment
COPY ./configs/config.yaml /usr/applications/avito_assignment/configs

RUN go mod download
RUN go build -o avito_assignment cmd/api/main.go

CMD ./avito_assignment