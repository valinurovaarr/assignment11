package config

import "github.com/spf13/viper"

type LocalConfig struct {
	Path string
}

func GetLocalConfig() (*LocalConfig, error) {

	storage := &LocalConfig{}
	err := viper.UnmarshalKey("localStorage", storage)

	if err != nil {
		return nil, err
	}

	return storage, nil
}
