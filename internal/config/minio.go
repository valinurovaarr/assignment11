package config

import "github.com/spf13/viper"

type MinioConfig struct {
	S3_BUCKET             string
	AWS_ACCESS_KEY_ID     string
	AWS_SECRET_ACCESS_KEY string
	MINIO_ENDPOINT        string
}

func GetMinioConfig() (*MinioConfig, error) {

	config := &MinioConfig{}
	err := viper.UnmarshalKey("minio", config)

	if err != nil {
		return nil, err
	}

	return config, nil
}
