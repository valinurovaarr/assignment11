package handler

import (
	_ "avito_assignment/docs"
	"avito_assignment/internal/core"
	"context"
	validator "github.com/go-playground/validator"
	"github.com/gofiber/fiber/v2"
	log "github.com/sirupsen/logrus"
	"net/http"
	"strconv"
)

type SegmentService interface {
	CreateSegment(ctx context.Context, segment *core.Segment) error
	DeleteSegment(ctx context.Context, name string) error
	AddToSegment(ctx context.Context, input *core.AddingInput) error
	GetActiveSegments(ctx context.Context, id int) ([]string, error)
	MakeReport(ctx context.Context, date string, userId int) (string, error)
}

type SegmentHandler struct {
	segmentService SegmentService
}

func NewSegmentHandler(service SegmentService) *SegmentHandler {
	return &SegmentHandler{segmentService: service}
}

func (handler *SegmentHandler) InitRoutes(app *fiber.App) {
	app.Post("/segments", handler.CreateSegment)
	app.Delete("/segments/:slug", handler.DeleteSegment)
	app.Post("/segments/addUser", handler.AddToSegment)
	app.Get("/segments/:userId", handler.GetActiveSegments)
	app.Get("/segments/report/:userId/:date", handler.MakeReport)
}

// @Summary Create new segment
// @Tags createSegment
// @Description create new segment
// @Accept  json
// @Produce  json
// @Param input body core.Segment true "segment info"
// @Success 201 {object} map[string]interface{}
// @Failure 400 {object} map[string]interface{}
// @Failure 500 {object} map[string]interface{}
// @Failure default {object} map[string]interface{}
// @Router /segments [post]
func (handler *SegmentHandler) CreateSegment(ctx *fiber.Ctx) error {

	segment := &core.Segment{}

	if err := ctx.BodyParser(segment); err != nil {
		log.Info(err.Error())
		return ctx.Status(http.StatusBadRequest).JSON(
			fiber.Map{
				"error parsing json": core.NewErrInvalidInputSyntax(),
			})
	}

	if ok, err := isSegmentValid(segment); !ok {
		log.Info(err.Error())
		return ctx.Status(http.StatusBadRequest).JSON(
			fiber.Map{
				"error validating object": err.Error(),
			})
	}

	log.WithFields(log.Fields{
		"name": segment.Name,
	}).Info("segment parsed")

	err := handler.segmentService.CreateSegment(ctx.UserContext(), segment)

	if err != nil {
		log.Info(err.Error())
		status, msg := setStatus(err)
		return ctx.Status(status).JSON(msg)
	}

	log.WithFields(log.Fields{
		"name": segment.Name,
	}).Info("segment saved")

	return ctx.Status(http.StatusCreated).JSON(
		fiber.Map{
			"createdSegment": segment.Name,
		})
}

// @Summary Delete existing segment
// @Tags deleteSegment
// @Description delete segment
// @Produce json
// @Param slug path string true "segment's slug(name)"
// @Success 200 {object} map[string]interface{}
// @Failure 400 {object} map[string]interface{}
// @Failure 500 {object} map[string]interface{}
// @Failure default {object} map[string]interface{}
// @Router /segments/{slug} [delete]
func (handler *SegmentHandler) DeleteSegment(ctx *fiber.Ctx) error {

	slug := ctx.Params("slug")
	_, err := strconv.Atoi(slug)
	if err == nil {
		return ctx.Status(http.StatusBadRequest).JSON(
			fiber.Map{
				"error": core.NewErrInvalidInputSyntax(),
			})
	}

	err = handler.segmentService.DeleteSegment(ctx.UserContext(), ctx.Params("slug"))

	if err != nil {
		log.Info(err.Error())
		status, msg := setStatus(err)
		return ctx.Status(status).JSON(msg)
	}

	log.WithFields(log.Fields{
		"name": ctx.Params("slug"),
	}).Info("segment deleted")

	return ctx.Status(http.StatusOK).JSON(
		fiber.Map{
			"deletedSegment": ctx.Params("slug"),
		})
}

// @Summary Add user to segment
// @Tags addToSegment
// @Description add user to segment
// @Accept  json
// @Produce  json
// @Param input body core.AddingInput true "add user to and delete from"
// @Success 200 {object} map[string]interface{}
// @Failure 400 {object} map[string]interface{}
// @Failure 500 {object} map[string]interface{}
// @Failure default {object} map[string]interface{}
// @Router /segments/addUser [post]
func (handler *SegmentHandler) AddToSegment(ctx *fiber.Ctx) error {

	opt := &core.AddingInput{}

	if err := ctx.BodyParser(opt); err != nil {
		log.Info(err.Error())
		return ctx.Status(http.StatusBadRequest).JSON(
			fiber.Map{
				"error": core.NewErrInvalidInputSyntax(),
			})
	}

	if ok, err := isAddingInputValid(opt); !ok {
		log.Info(err.Error())
		return ctx.Status(http.StatusBadRequest).JSON(
			fiber.Map{
				"error": err.Error(),
			})
	}

	log.WithFields(log.Fields{
		"userId":  opt.UserId,
		"add":     opt.Add,
		"delete":  opt.Delete,
		"exptime": opt.ExpTime,
	}).Info("input parsed")

	err := handler.segmentService.AddToSegment(ctx.UserContext(), opt)

	if err != nil {
		log.Info(err.Error())
		status, msg := setStatus(err)
		return ctx.Status(status).JSON(msg)
	}

	log.WithFields(log.Fields{
		"userId":  opt.UserId,
		"add":     opt.Add,
		"delete":  opt.Delete,
		"exptime": opt.ExpTime,
	}).Info("user added to segment")

	return ctx.Status(http.StatusOK).JSON(
		fiber.Map{
			"userId":  opt.UserId,
			"add":     opt.Add,
			"delete":  opt.Delete,
			"exptime": opt.ExpTime,
		})

}

// GetActiveUserSegments
// @Summary Get user's active segments
// @Tags activeSegments
// @Description Returns list of user's active segments
// @Produce json
// @Param userId path int true "user's active segments"
// @Success 200 {object} map[string]interface{}
// @Failure 400 {object} map[string]interface{}
// @Failure 500 {object} map[string]interface{}
// @Router /segments/{userId} [get]
func (handler *SegmentHandler) GetActiveSegments(ctx *fiber.Ctx) error {

	userId, err := strconv.Atoi(ctx.Params("userId"))
	if err != nil {
		log.Info(err.Error())
		return ctx.Status(http.StatusBadRequest).JSON(
			fiber.Map{
				"error": core.NewErrInvalidInputSyntax(),
			})
	}

	log.WithFields(log.Fields{
		"id": userId,
	}).Info("users id parsed")

	segments, err := handler.segmentService.GetActiveSegments(ctx.UserContext(), userId)
	if err != nil {
		log.Info(err.Error())
		status, msg := setStatus(err)
		return ctx.Status(status).JSON(msg)
	}

	log.WithFields(log.Fields{
		"userId":          userId,
		"active segments": segments,
	}).Info("got active segments")

	return ctx.Status(http.StatusOK).JSON(
		fiber.Map{
			"userId":          userId,
			"active segments": segments,
		})
}

// @Summary Create report
// @Tags createReport
// @Description create user activity reoprt
// @Produce  json
// @Param userId path int true "user"
// @Param date path string true "date of report"
// @Success 201 {object} map[string]interface{}
// @Failure 400 {object} map[string]interface{}
// @Failure 500 {object} map[string]interface{}
// @Failure default {object} map[string]interface{}
// @Router /segments/report/{userId}/{date} [get]
func (handler *SegmentHandler) MakeReport(ctx *fiber.Ctx) error {

	userId, err := strconv.Atoi(ctx.Params("userId"))
	if err != nil {
		log.Info(err.Error())
		return ctx.Status(http.StatusBadRequest).JSON(
			fiber.Map{
				"error": core.NewErrInvalidInputSyntax(),
			})
	}

	link, err := handler.segmentService.MakeReport(ctx.UserContext(), ctx.Params("date"), userId)
	if err != nil {
		log.Info(err.Error())
		status, msg := setStatus(err)
		return ctx.Status(status).JSON(msg)
	}

	return ctx.Status(http.StatusCreated).JSON(
		fiber.Map{
			"link": link,
		})
}

func setStatus(err error) (int, interface{}) {

	if err == nil {
		return http.StatusOK, nil
	}

	if core.Is(err) {
		res, _ := err.(*core.MyError)
		return res.Inf.StatusCode, map[string]interface{}{
			"error": err.Error(),
		}
	}

	return http.StatusInternalServerError, map[string]interface{}{
		"error": "internal server error",
	}

}

func isSegmentValid(segment *core.Segment) (bool, error) {
	validate := validator.New()
	err := validate.Struct(segment)
	if err != nil {
		log.Info(err.Error())
		return false, core.NewErrInvalidInputObject()
	}
	return true, nil
}

func isAddingInputValid(input *core.AddingInput) (bool, error) {
	validate := validator.New()
	err := validate.Struct(input)
	if err != nil {
		log.Info(err.Error())
		return false, core.NewErrInvalidInputObject()
	}

	if len(input.Add) == 0 && len(input.Delete) == 0 {
		log.Info("invalid json input: empty arrays")
		return false, core.NewErrNothinToAdd()
	}

	return true, nil
}
