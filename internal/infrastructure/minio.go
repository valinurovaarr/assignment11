package infrastructure

import (
	"avito_assignment/internal/config"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

func SetUpS3Storage(config *config.MinioConfig) (*minio.Client, error) {

	minioClient, err := minio.New(config.MINIO_ENDPOINT, &minio.Options{Creds: credentials.NewStaticV4(
		config.AWS_ACCESS_KEY_ID, config.AWS_SECRET_ACCESS_KEY, ""), Secure: false})

	if err != nil {
		return nil, err
	}

	return minioClient, nil
}
