package service

import (
	"avito_assignment/internal/core"
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"strconv"
	"strings"
	"time"
)

type SegmentRepository interface {
	CreateSegment(ctx context.Context, segment *core.Segment, percent float32) error
	DeleteSegment(ctx context.Context, name string) error
	AddUserToSegment(ctx context.Context, input *core.AddingInput) error
	GetActiveUsersSegments(ctx context.Context, id int, moment time.Time) ([]*core.Segment, error)
	CreateReport(ctx context.Context, year string, month string, id int) ([]*core.Record, error)
	DeleteExpiredUsers(time time.Time) error
}

type LocalRepository interface {
	SaveReport(records []*core.Record) (string, error)
}

type MinioRepository interface {
	StoreReport(ctx context.Context, path string, name string) (string, error)
}

type SegmentService struct {
	segmentRepository SegmentRepository
	localRepository   LocalRepository
	MinioRepository   MinioRepository
}

func NewSegmentService(segmentRepository SegmentRepository, localRepository LocalRepository, minioRepository MinioRepository) *SegmentService {
	return &SegmentService{segmentRepository: segmentRepository, localRepository: localRepository, MinioRepository: minioRepository}
}

func (service *SegmentService) CreateSegment(ctx context.Context, segment *core.Segment) error {
	percent := float32(segment.UserPercent) / 100
	return service.segmentRepository.CreateSegment(ctx, segment, percent)
}

func (service *SegmentService) DeleteSegment(ctx context.Context, slug string) error {
	return service.segmentRepository.DeleteSegment(ctx, slug)
}

func (service *SegmentService) AddToSegment(ctx context.Context, input *core.AddingInput) error {
	str := input.ExpTime.Format("2006-01-02 3:4:5")
	newTime, err := time.Parse("2006-01-02 3:4:5", str)
	if err != nil {
		return err
	}

	input.ExpTime = newTime
	return service.segmentRepository.AddUserToSegment(ctx, input)
}

func (service *SegmentService) GetActiveSegments(ctx context.Context, id int) ([]string, error) {
	var response []string
	moment := time.Now()
	segments, err := service.segmentRepository.GetActiveUsersSegments(ctx, id, moment)
	if err != nil {
		return nil, err
	}

	if len(segments) == 0 {
		return nil, core.NewErrNoActiveUserSegments()
	}

	for _, s := range segments {
		response = append(response, s.Name)
	}

	return response, err
}

func (service *SegmentService) MakeReport(ctx context.Context, date string, userId int) (string, error) {

	var records []*core.Record

	res := strings.Split(date, "-")
	year := res[0]
	month := res[1]

	records, err := service.segmentRepository.CreateReport(ctx, year, month, userId)
	if err != nil {
		return "", fmt.Errorf("Error creating report: %w", err)
	}

	file, err := service.localRepository.SaveReport(records)
	if err != nil {
		return "nil", fmt.Errorf("Error locally saving report: %w", err)
	}

	name := "user" + strconv.Itoa(userId) + "_report_" + date

	link, err := service.MinioRepository.StoreReport(ctx, file, name)
	if err != nil {
		return "nil", fmt.Errorf("Error locally saving report: %w", err)
	}

	link = strings.Replace(link, "s3", "localhost", -1)
	link = strings.Replace(link, "9000", "9001", -1)

	return link, nil
}

func (service *SegmentService) ControlExpiredUsers() {
	currentTime := time.Now()
	err := service.segmentRepository.DeleteExpiredUsers(currentTime)
	if err != nil {
		log.Info(err.Error())
	}
}
