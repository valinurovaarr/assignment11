package postgres

import (
	"avito_assignment/internal/core"
	"context"
	"database/sql"
	"errors"
	"fmt"
	"github.com/jackc/pgx/v5/pgconn"
	"github.com/jmoiron/sqlx"
	log "github.com/sirupsen/logrus"
	"time"
)

type SegmentRepository struct {
	Db *sqlx.DB
}

const (
	CreateSegment           = "INSERT INTO segments (slug) values ($1);"
	AddRandomUsersToSegment = "with t1 as (select pk from users where random() < ($1)), t2 as (select pk from segments where slug = ($2)) insert into user_segment(user_id, segment_id) select t1.pk, t2.pk from t1, t2;"
	DeleteSegment           = "DELETE FROM segments where slug = ($1);"
	AddUserToSegment        = "INSERT INTO user_segment(user_id, segment_id, exp_time) select ($1), pk, ($2) from segments where slug =any ($3);"
	DeleteSegmentFromUser   = "DELETE FROM user_segment where user_id = ($1) AND segment_id = (select pk from segments where slug =any ($2) );"
	GetActiveUsersSegments  = "SELECT slug FROM segments inner join user_segment on pk(segments)=segment_id(user_segment) where (exp_time is null AND user_id=($1)) OR (exp_time < ($2) AND user_id=($1));"

	AddtoHistory = "INSERT INTO operations(user_id, datetime, status, segment_id) select $1, $2, $3, pk from segments where slug =any ($4);"
	CreateReport = "SELECT user_id, slug, status, datetime FROM operations inner join segments on segment_id(operations)=pk(segments) where EXTRACT(YEAR FROM datetime) = ($1) AND EXTRACT(MONTH FROM datetime) = ($2) AND user_id = ($3);"

	DeleteExpiredUsers = "DELETE FROM user_segment where exp_time < ($1);"

	UniqueViolationErr  = "23505"
	ForeignKeyViolation = "23503"
)

func NewSegmentRepository(db *sqlx.DB) *SegmentRepository {
	return &SegmentRepository{Db: db}
}

var perr *pgconn.PgError

func (repository *SegmentRepository) CreateSegment(ctx context.Context, segment *core.Segment, percent float32) error {

	tx, err := repository.Db.Begin()
	if err != nil {
		return fmt.Errorf("Error beginning transaction: %w", err)
	}

	defer tx.Rollback()

	_, err = tx.ExecContext(ctx, CreateSegment, segment.Name)

	if errors.As(err, &perr) && perr.Code == UniqueViolationErr {
		return core.NewErrSegmentAlreadyExists()
	}

	if err != nil {
		return fmt.Errorf("Error creating new segment: %w", err)
	}

	_, err = tx.ExecContext(ctx, AddRandomUsersToSegment, percent, segment.Name)

	if err = tx.Commit(); err != nil {
		return fmt.Errorf("Error commiting transaction: %w", err)
	}

	return nil

}

func (repository *SegmentRepository) DeleteSegment(ctx context.Context, slug string) error {

	res, err := repository.Db.ExecContext(ctx, DeleteSegment, slug)
	if errors.As(err, &perr) && perr.Code == ForeignKeyViolation {
		return core.NewErrStillHaveRelations()
	}

	if err != nil {
		return fmt.Errorf("Error deleting segment %w", err)
	}

	rows, err := res.RowsAffected()
	if err != nil {
		return fmt.Errorf("Error deleting segment %w", err)
	}

	if rows == 0 {
		return core.NewErrSegmentDoesntExist()
	}

	return nil
}

func (repository *SegmentRepository) AddUserToSegment(ctx context.Context, input *core.AddingInput) error {

	var err error
	var res sql.Result
	var rows int64

	tx, err := repository.Db.Begin()
	if err != nil {
		return fmt.Errorf("Error beginning transaction: %w", err)
	}

	defer tx.Rollback()

	if len(input.Add) > 0 {
		_, err = tx.ExecContext(ctx, AddUserToSegment, input.UserId, input.ExpTime, input.Add)
		if err != nil {
			if errors.As(err, &perr) {
				switch perr.Code {
				case UniqueViolationErr:
					return core.NewErrRelationAlreadyExists()
				case ForeignKeyViolation:
					log.Info(err.Error())
					return core.NewErrUserOrSegmentDoesntExist()
				default:
					return fmt.Errorf("Unexpected postgres error: %w", err)
				}
			}
			return fmt.Errorf("Error adding user to segment: %w", err)
		}

		currentTime := time.Now().Format("2006-01-02 3:4:5")

		res, err = tx.ExecContext(ctx, AddtoHistory, input.UserId, currentTime, true, input.Add)
		if err != nil {
			return fmt.Errorf("Error recording adding to the history %w", err)
		}

		rows, err = res.RowsAffected()
		if rows == 0 {
			return core.NewErrUserOrSegmentDoesntExist()
		}
	}

	if len(input.Delete) > 0 {
		res, err = tx.ExecContext(ctx, DeleteSegmentFromUser, input.UserId, input.Delete)
		if err != nil {
			return fmt.Errorf("Error deleting segment %w", err)
		}

		rows, err = res.RowsAffected()
		if err != nil {
			return fmt.Errorf("Error deleting segment %w", err)
		}

		if rows == 0 {
			return core.NewErrUserSegmentOrRelationDoesntExist()
		}

		currentTime := time.Now().Format("2006-01-02 3:4:5")

		res, err = tx.ExecContext(ctx, AddtoHistory, input.UserId, currentTime, false, input.Delete)
		if err != nil {
			return fmt.Errorf("Error recording deleting to the history %w", err)
		}

		rows, err = res.RowsAffected()
		if rows == 0 {
			return fmt.Errorf("No rows after deleting")
		}
	}

	if err = tx.Commit(); err != nil {
		return fmt.Errorf("Error commiting transaction: %w", err)
	}

	return nil

}

func (repository *SegmentRepository) GetActiveUsersSegments(ctx context.Context, userId int, moment time.Time) ([]*core.Segment, error) {

	var activeSegments []*core.Segment

	rows, err := repository.Db.QueryContext(ctx, GetActiveUsersSegments, userId, moment.Format("2006-01-02 3:4:5"))
	if err != nil {
		return nil, fmt.Errorf("Error getting segments: %w", err)
	}

	defer rows.Close()

	for rows.Next() {
		segment := &core.Segment{}
		err = rows.Scan(&segment.Name)
		if err != nil {
			return nil, fmt.Errorf("Error getting segment: %w", err)
		}
		activeSegments = append(activeSegments, segment)
	}

	return activeSegments, nil
}

func (repository *SegmentRepository) CreateReport(ctx context.Context, year string, month string, userId int) ([]*core.Record, error) {

	var records []*core.Record

	rows, err := repository.Db.QueryContext(ctx, CreateReport, year, month, userId)
	if err == sql.ErrNoRows {
		fmt.Errorf("Error retrieving rows: %w", err)
	}
	if err != nil {
		return nil, fmt.Errorf("Error getting segments: %w", err)
	}

	defer rows.Close()

	for rows.Next() {
		record := &core.Record{}
		err = rows.Scan(&record.UserId, &record.Slug, &record.Status, &record.Datetime)
		if err != nil {
			return nil, fmt.Errorf("Error getting record: %w", err)
		}
		records = append(records, record)
	}

	return records, nil
}

func (repository *SegmentRepository) DeleteExpiredUsers(time time.Time) error {
	_, err := repository.Db.Exec(DeleteExpiredUsers, time.Format("2006-01-02 3:4:5"))
	if err != nil {
		return fmt.Errorf("Error deleting expired user %w", err)
	}

	return nil
}
