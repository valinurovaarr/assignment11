package repository

import (
	"context"
	"fmt"
	"github.com/minio/minio-go/v7"
	"net/url"
	"time"
)

type MinioRepository struct {
	S3        *minio.Client
	S3_bucket string
}

func NewMinioRepository(client *minio.Client, bucket string) *MinioRepository {
	return &MinioRepository{S3: client, S3_bucket: bucket}
}

func (repository *MinioRepository) StoreReport(ctx context.Context, path string, name string) (string, error) {

	newName := name + ".csv"
	fileName := path

	_, err := repository.S3.FPutObject(ctx, repository.S3_bucket, newName, fileName,
		minio.PutObjectOptions{ContentType: "text/csv"})
	if err != nil {
		return "", fmt.Errorf("Error storing report in s3: %w", err)
	}

	reqParams := make(url.Values)
	reqParams.Set("response-content-disposition", "attachment; filename=\""+newName+"\"")

	link, err := repository.S3.PresignedGetObject(ctx, repository.S3_bucket, newName, time.Duration(2)*time.Hour, reqParams)
	if err != nil {
		return "", fmt.Errorf("Error getting link: %w", err)
	}

	return link.String(), nil
}
