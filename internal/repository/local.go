package repository

import (
	"avito_assignment/internal/core"
	"encoding/csv"
	"fmt"
	"github.com/fatih/structs"
	"os"
)

type LocalRepository struct {
	Path string
}

func NewLocalRepository(path string) *LocalRepository {
	return &LocalRepository{Path: path}
}

func (repository *LocalRepository) SaveReport(records []*core.Record) (string, error) {

	records2 := [][]string{
		{"user_id", "slug", "operation", "datetime"},
	}

	for _, s := range records {
		temp := structs.Values(&s)
		temp = append(temp, s.Datetime.Format("2006-01-02 3:4:5"))
		var strings []string
		for _, n := range temp {
			str := fmt.Sprintf("%v", n)
			strings = append(strings, str)
		}
		records2 = append(records2, strings)
	}

	f, err := os.OpenFile(repository.Path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		return "", err
	}

	w := csv.NewWriter(f)
	w.WriteAll(records2)

	if err = w.Error(); err != nil {
		return "", err
	}

	return repository.Path, nil
}
