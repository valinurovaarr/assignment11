package core

import (
	"fmt"
	"net/http"
)

type Info struct {
	Msg        string
	StatusCode int
}

type MyError struct {
	Inf  Info
	Type string
}

func (err *MyError) Error() string {
	return fmt.Sprintf("message:'%s'", err.Inf.Msg)
}

func NewErrSegmentDoesntExist() *MyError {
	return &MyError{Type: "ErrSegmentDoesntExist", Inf: Info{Msg: "segment with this name do not exist", StatusCode: http.StatusBadRequest}}
}

func NewErrSegmentAlreadyExists() *MyError {
	return &MyError{Type: "ErrSegmentAlreadyExists", Inf: Info{Msg: "segment already exists", StatusCode: http.StatusBadRequest}}
}

func NewErrNoActiveUserSegments() *MyError {
	return &MyError{Type: "ErrNoActiveUserSegments", Inf: Info{Msg: "no active user segments or user does not exist", StatusCode: http.StatusNotFound}}
}

func NewErrRelationAlreadyExists() *MyError {
	return &MyError{Type: "ErrRelationAlreadyExists", Inf: Info{Msg: "user - segment relation already exists", StatusCode: http.StatusBadRequest}}
}

func NewErrUserOrSegmentDoesntExist() *MyError {
	return &MyError{Type: "ErrUserOrSegmentDoesntExist", Inf: Info{Msg: "rather user or segment does not exist", StatusCode: http.StatusBadRequest}}
}

func NewErrUserSegmentOrRelationDoesntExist() *MyError {
	return &MyError{Type: "ErrUserSegmentOrRelationDoesntExist", Inf: Info{Msg: "user or segment or relation does not exist", StatusCode: http.StatusBadRequest}}
}

func NewErrStillHaveRelations() *MyError {
	return &MyError{Type: "ErrStillHaveRelations", Inf: Info{Msg: "this segment still have  user - segment relations", StatusCode: http.StatusBadRequest}}
}

func NewErrInvalidInputSyntax() *MyError {
	return &MyError{Type: "ErrInvalidInputSyntax", Inf: Info{Msg: "invalid input syntax", StatusCode: http.StatusBadRequest}}
}

func NewErrInvalidInputObject() *MyError {
	return &MyError{Type: "ErrInvalidInputObject", Inf: Info{Msg: "invalid input object", StatusCode: http.StatusBadRequest}}
}

func NewErrNothinToAdd() *MyError {
	return &MyError{Type: "ErrNothinToAdd", Inf: Info{Msg: "nothing to add or delete", StatusCode: http.StatusBadRequest}}
}

func Is(tgt error) bool {
	_, ok := tgt.(*MyError)
	if !ok {
		return false
	}
	return true
}
