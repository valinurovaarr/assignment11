package core

import "time"

type Segment struct {
	Id          int    `json:"id,omitempty" swaggerignore:"true"`
	Name        string `json:"name" validate:"required"`
	UserPercent int    `json:"percent" validate:"omitempty,gt=0,lte=100"`
}

type Record struct {
	UserId   int
	Slug     string
	Status   bool
	Datetime time.Time
}

type AddingInput struct {
	UserId  int       `json:"userId" validate:"required"`
	Add     []string  `json:"add" validate:"required"`
	Delete  []string  `json:"delete" validate:"required"`
	ExpTime time.Time `json:"time,omitempty"`
}
