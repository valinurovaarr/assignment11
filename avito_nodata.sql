--
-- PostgreSQL database dump
--

-- Dumped from database version 15.4
-- Dumped by pg_dump version 15.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: operations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.operations (
    pk integer NOT NULL,
    user_id integer NOT NULL,
    datetime timestamp without time zone NOT NULL,
    status boolean,
    segment_id integer NOT NULL
);


ALTER TABLE public.operations OWNER TO postgres;

--
-- Name: operations_pk_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.operations_pk_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.operations_pk_seq OWNER TO postgres;

--
-- Name: operations_pk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.operations_pk_seq OWNED BY public.operations.pk;


--
-- Name: segments; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.segments (
    pk integer NOT NULL,
    slug character varying NOT NULL
);


ALTER TABLE public.segments OWNER TO postgres;

--
-- Name: segments_pk_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.segments_pk_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.segments_pk_seq OWNER TO postgres;

--
-- Name: segments_pk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.segments_pk_seq OWNED BY public.segments.pk;


--
-- Name: user_segment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_segment (
    user_id integer NOT NULL,
    segment_id integer NOT NULL,
    exp_time timestamp without time zone
);


ALTER TABLE public.user_segment OWNER TO postgres;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    pk integer NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE public.users OWNER TO postgres;

--
-- Name: users_pk_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_pk_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_pk_seq OWNER TO postgres;

--
-- Name: users_pk_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_pk_seq OWNED BY public.users.pk;


--
-- Name: operations pk; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.operations ALTER COLUMN pk SET DEFAULT nextval('public.operations_pk_seq'::regclass);


--
-- Name: segments pk; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.segments ALTER COLUMN pk SET DEFAULT nextval('public.segments_pk_seq'::regclass);


--
-- Name: users pk; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN pk SET DEFAULT nextval('public.users_pk_seq'::regclass);


--
-- Name: operations operations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.operations
    ADD CONSTRAINT operations_pkey PRIMARY KEY (pk);


--
-- Name: segments segments_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.segments
    ADD CONSTRAINT segments_pkey PRIMARY KEY (pk);


--
-- Name: segments unique_slug; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.segments
    ADD CONSTRAINT unique_slug UNIQUE (slug);


--
-- Name: user_segment unique_user_segment_relation; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_segment
    ADD CONSTRAINT unique_user_segment_relation UNIQUE (user_id, segment_id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (pk);


--
-- Name: user_segment fk_segment; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_segment
    ADD CONSTRAINT fk_segment FOREIGN KEY (segment_id) REFERENCES public.segments(pk);


--
-- Name: operations fk_segment; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.operations
    ADD CONSTRAINT fk_segment FOREIGN KEY (segment_id) REFERENCES public.segments(pk);


--
-- Name: operations fk_user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.operations
    ADD CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES public.users(pk);


--
-- Name: user_segment fk_user; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_segment
    ADD CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES public.users(pk);


--
-- PostgreSQL database dump complete
--

